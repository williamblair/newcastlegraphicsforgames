#version 150 core

uniform samplerCube cubeTex;

in Vertex
{
    vec3 normal;
} IN;

out vec4 fragColour;

void main()
{
    fragColour = vec4(texture(cubeTex, normalize(IN.normal)).rgb,1.0);
}

