CXX = g++
CC = gcc
CXXFLAGS = -g -Wall -std=c++17 -D_DEBUG
CFLAGS = -g -Wall -D_DEBUG
INCDIRS = -I./src -I./
LIBDIRS = 
#LIBS = -lSDL2 -lSDL2main -lGLEW -lGLU -lGL -lSOIL
LIBS = -lSDL2 -lSDL2main -lGLEW -lGLU -lGL
CPPSOURCES = $(wildcard ./src/*.cpp) $(wildcard ./nclgl/*.cpp)
CSOURCES =
OBJS = $(CPPSOURCES:.cpp=.o) $(CSOURCES:.c=.o)
TARGET = main

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $< $(INCDIRS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $< $(INCDIRS)

all: $(OBJS)
	$(CXX) $(CFLAGS) -o $(TARGET) $(OBJS) $(INCDIRS) $(LIBDIRS) $(LIBS)

clean:
	rm -f $(OBJS) $(TARGET)
