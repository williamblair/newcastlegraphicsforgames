#include "MD5Node.h"
#include "MD5FileData.h"
#include "MD5Mesh.h"

MD5Node::MD5Node(const MD5FileData& ofType) :
    sourceData(ofType)
{
    currentAnim = nullptr;
    frameTime = 0.0f;
    currentAnimFrame = 0;
    ofType.CloneSkeleton(currentSkeleton);
    mesh = ofType.GetRootMesh();
}
MD5Node::~MD5Node()
{
}

void MD5Node::Update(float msec)
{
    if (!currentAnim) {
        SceneNode::Update(msec);
        return;
    }

    frameTime -= msec;
    while (frameTime < 0.0f) {
        frameTime += 1000.0f/currentAnim->GetFrameRate();
        currentAnimFrame = (currentAnimFrame+1)%(currentAnim->GetNumFrames());
    }
    currentAnim->TransformSkeleton(currentSkeleton,currentAnimFrame-1);
    SceneNode::Update(msec);
}
void MD5Node::Draw(const OGLRenderer& r)
{
    MD5Mesh* m = (MD5Mesh*)mesh;
    m->SkinVertices(currentSkeleton);
    m->Draw();
}

void MD5Node::PlayAnim(std::string name)
{
    // reset anim details
    currentAnimFrame = 0;
    frameTime = 0.0f;
    currentAnim = sourceData.GetAnim(name);
}

