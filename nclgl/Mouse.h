#ifndef NCLGL_MOUSE_H_INCLUDED
#define NCLGL_MOUSE_H_INCLUDED

#include <unordered_map>
#include <SDL2/SDL.h>

#include "InputDevice.h"
#include "Vector2.h"

enum MouseButtons
{
    MOUSE_LEFT = SDL_BUTTON_LEFT,
    MOUSE_RIGHT = SDL_BUTTON_RIGHT,
    MOUSE_MIDDLE = SDL_BUTTON_MIDDLE,
    MOUSE_FOUR = SDL_BUTTON_X1,
    MOUSE_FIVE = SDL_BUTTON_X2
};

class Mouse : public InputDevice
{
public:
    friend class Window;

    bool ButtonDown(MouseButtons button);
    bool ButtonHeld(MouseButtons button);
    bool DoubleClicked(MouseButtons button);

    Vector2 GetRelativePosition();
    Vector2 GetAbsolutePosition();

    void SetDoubleClickLimit(float msec);
    bool WheelMoved();
    int GetWheelMovement();
    void SetMouseSensitivity(float amount);

protected:
    Mouse();
    virtual ~Mouse() {}

    virtual void Update(const SDL_Event& e);
    virtual void UpdateHolds();
    virtual void Sleep();

    void UpdateDoubleClick(float msec);
    void SetAbsolutePosition(unsigned int x, unsigned int y);
    void SetAbsolutePositionBounds(unsigned int maxX, unsigned int maxY);

    Vector2 absolutePosition;
    Vector2 absolutePositionBounds;
    Vector2 relativePosition;
    std::unordered_map<MouseButtons,bool> buttons;
    std::unordered_map<MouseButtons,bool> holdButtons;
    std::unordered_map<MouseButtons,bool> doubleClicks;
    std::unordered_map<MouseButtons,float> lastClickTime;
    int lastWheel;
    int frameWheel;
    float clickLimit;
    float sensitivity;
};

#endif // NCLGL_MOUSE_H_INCLUDED

