#include "Window.h"

// static member variables
Window* Window::window = nullptr;
Keyboard* Window::keyboard = nullptr;
Mouse* Window::mouse = nullptr;

Window::Window(std::string title, int sizeX, int sizeY, bool fullScreen)
{
    sdlWindow = nullptr;
    renderer = nullptr;
    window = this;
    forceQuit = false;
    init = false;
    mouseLeftWindow = false;
    lockMouse = false;
    showMouse = true;

    this->fullScreen = fullScreen;

    size.x = float(sizeX);
    size.y = float(sizeY);

    fullScreen ? position.x = 0.0f : position.x = 100.0f;
    fullScreen ? position.y = 0.0f : position.y = 100.0f;

    //TODO
    if (!keyboard) {
        keyboard = new Keyboard();
    }
    if (!mouse) {
        mouse = new Mouse();
    }
    //timer = new GameTimer();
    //elapsedMS = timer->GetMS();

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO) < 0) {
        printf("SDL init failed: %s\n", SDL_GetError());
        throw std::runtime_error("SDL Init failed");
    }
    sdlWindow = SDL_CreateWindow(
        title.c_str(),
        position.x, position.y,
        size.x, size.y,
        SDL_WINDOW_OPENGL
    );
    if (!sdlWindow) {
        printf("SDL Create Window failed: %s\n", SDL_GetError());
        throw std::runtime_error("SDL Create Window failed");
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // create a render context
    sdlContext = SDL_GL_CreateContext(sdlWindow);
    
    // enable vsync
    SDL_GL_SetSwapInterval(1);

    // hide/capture mouse cursor
    SDL_SetRelativeMouseMode(SDL_TRUE);

    LockMouseToWindow(lockMouse);
    ShowOSPointer(showMouse);

    init = true;
}
Window::~Window()
{
    delete keyboard; keyboard = nullptr;
    delete mouse; mouse = nullptr;

    if (sdlWindow) {
        SDL_GL_DeleteContext(sdlContext);
        SDL_DestroyWindow(sdlWindow);
        sdlWindow = nullptr;
        SDL_Quit();
    }
}

bool Window::UpdateWindow()
{
    //TODO
    float diff = 1000.0f/60.0f;

    //TODO
    Window::GetMouse()->UpdateDoubleClick(diff);
    Window::GetKeyboard()->UpdateHolds();
    Window::GetMouse()->UpdateHolds();

    SDL_Event e;
    while (SDL_PollEvent(&e))
    {
        if (e.type == SDL_QUIT) {
            window->ShowOSPointer(true);
            window->LockMouseToWindow(false);
            forceQuit = true;
        }
        else if (e.type == SDL_KEYDOWN || e.type == SDL_KEYUP) {
            Window::GetKeyboard()->Update(e);
        }
        else if (e.type == SDL_MOUSEBUTTONDOWN ||
                e.type == SDL_MOUSEBUTTONUP ||
                e.type == SDL_MOUSEMOTION ||
                e.type == SDL_MOUSEWHEEL) {
            Window::GetMouse()->Update(e);
        }
    }

    //TODO
    //elapsedMS = timer->GetMS();

    return !forceQuit;
}
void Window::SetRenderer(OGLRenderer* r)
{
    renderer = r;
    if (r) {
        renderer->Resize(int(size.x), int(size.y));
    }
}

bool Window::HasInitialised()
{
    return init;
}

void Window::LockMouseToWindow(bool lock)
{
    //TODO
}

void Window::ShowOSPointer(bool show)
{
    //TODO
}

