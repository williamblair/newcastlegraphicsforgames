#include <cassert>
#include "Keyboard.h"

Keyboard::Keyboard()
{
}

void Keyboard::UpdateHolds()
{
    holdStates = keyStates;
}

void Keyboard::Sleep()
{
    isAwake = false;
    for (auto& pair : keyStates) {
        pair.second = false;
    }
    for (auto& pair : holdStates) {
        pair.second = false;
    }
}

bool Keyboard::KeyDown(KeyboardKeys key)
{
    return keyStates[key];
}

bool Keyboard::KeyHeld(KeyboardKeys key)
{
    if (KeyDown(key) && holdStates[key]) {
        return true;
    }
    return false;
}

bool Keyboard::KeyTriggered(KeyboardKeys key)
{
    return KeyDown(key) && !KeyHeld(key);
}

void Keyboard::Update(const SDL_Event& e)
{
    if (!isAwake) {
        return;
    }

    if (e.type == SDL_KEYDOWN) {
        keyStates[KeyboardKeys(e.key.keysym.sym)] = true;
    } else if (e.type == SDL_KEYUP) {
        keyStates[KeyboardKeys(e.key.keysym.sym)] = false;
    }
}

