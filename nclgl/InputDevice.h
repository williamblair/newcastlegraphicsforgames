#ifndef NCLGL_INPUT_DEVICE_H_INCLUDED
#define NCLGL_INPUT_DEVICE_H_INCLUDED

#include <SDL2/SDL.h>

class Window;

class InputDevice
{
protected:
    friend class Window;
    InputDevice() { isAwake = true; }
    virtual ~InputDevice() {}

    virtual void Update(const SDL_Event& e) = 0;
    virtual void Sleep() { isAwake = false; }
    virtual void Wake() { isAwake = true; }

    bool isAwake;
};

#endif // NCLGL_INPUT_DEVICE_H_INCLUDED

