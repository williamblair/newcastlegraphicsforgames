#pragma once

#include "SceneNode.h"
#include "MD5FileData.h"
#include "MD5Anim.h"

class MD5Node : public SceneNode
{
public:
    MD5Node(const MD5FileData& ofType);
    virtual ~MD5Node();

    virtual void Update(float msec);
    virtual void Draw(const OGLRenderer& r);

    void PlayAnim(std::string name);

protected:
    const MD5FileData& sourceData;
    MD5Skeleton currentSkeleton;
    MD5Anim* currentAnim;

    float frameTime;
    unsigned int currentAnimFrame;
};

