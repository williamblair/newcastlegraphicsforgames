#ifndef NCLGL_MESH_H_INCLUDED
#define NCLGL_MESH_H_INCLUDED

#include "OGLRenderer.h"

enum MeshBuffer {
    VERTEX_BUFFER,
    COLOUR_BUFFER,
    TEXTURE_BUFFER,
    NORMAL_BUFFER,
    TANGENT_BUFFER,
    INDEX_BUFFER,
    MAX_BUFFER
};

class Mesh
{
public:
    friend class MD5FileData;

    Mesh();
    virtual ~Mesh();

    virtual void Draw();
    static Mesh* GenerateTriangle();
    static Mesh* GenerateQuad();

    void SetTexture(GLuint tex) { texture = tex; }
    GLuint GetTexture() { return texture; }

    void SetBumpMap(GLuint tex) { bumpTexture = tex; }
    GLuint GetBumpMap() { return bumpTexture; }

protected:
    void GenerateNormals();
    void GenerateTangents();
    Vector3 GenerateTangent(
        const Vector3& a, const Vector3& b, const Vector3& c,
        const Vector2& ta, const Vector2& tb, const Vector2& tc
    );
    void BufferData();

    GLuint arrayObject;
    GLuint bufferObject[MAX_BUFFER];
    GLuint numVertices;
    GLuint numIndices;
    GLuint type;
    GLuint texture;
    GLuint bumpTexture;

    Vector3* vertices;
    Vector4* colours;
    Vector2* textureCoords;
    Vector3* normals;
    Vector3* tangents;
    unsigned int* indices;
};

#endif // NCLGL_MESH_H_INCLUDED

