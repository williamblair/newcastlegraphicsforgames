#include "Mouse.h"

Mouse::Mouse()
{
    lastWheel = 0;
    frameWheel = 0;
    sensitivity = 0.07f;
    clickLimit = 200.0f;
}

void Mouse::Update(const SDL_Event& e)
{
    if (!isAwake) {
        return;
    }

    if (e.type == SDL_MOUSEMOTION) {
        relativePosition.x = e.motion.xrel * sensitivity;
        relativePosition.y = e.motion.yrel * sensitivity;

        absolutePosition.x = e.motion.x;
        absolutePosition.y = e.motion.y;
    }

    else if (e.type == SDL_MOUSEWHEEL) {
        if (e.wheel.y > 0) { frameWheel = 1; }
        else { frameWheel = -1; }
    }

    else if (e.type == SDL_MOUSEBUTTONDOWN) {
        MouseButtons idx = (MouseButtons)(e.button.button);
        buttons[idx] = true;
        if (lastClickTime[idx] > 0) {
            doubleClicks[idx] = true;
        }
        lastClickTime[idx] = clickLimit;
    }

    else if (e.type == SDL_MOUSEBUTTONUP) {
        buttons[MouseButtons(e.button.button)] = false;
        holdButtons[MouseButtons(e.button.button)] = false;
    }
}

void Mouse::SetMouseSensitivity(float amount)
{
    if (amount == 0.0f) {
        amount = 1.0f;
    }
    sensitivity = amount;
}

void Mouse::UpdateHolds()
{
    holdButtons = buttons;
    relativePosition.ToZero();
    frameWheel = 0;
}

void Mouse::Sleep()
{
    isAwake = false;
    for (auto& pair : holdButtons) {
        pair.second = false;
    }
    for (auto& pair : buttons) {
        pair.second = false;
    }
}

void Mouse::SetAbsolutePosition(unsigned int x, unsigned int y) {
    absolutePosition.x = float(x);
    absolutePosition.y = float(y);
}

bool Mouse::ButtonDown(MouseButtons b) {
    return buttons[b];
}

bool Mouse::ButtonHeld(MouseButtons b) {
    return holdButtons[b];
}

Vector2 Mouse::GetRelativePosition() {
    return relativePosition;
}

Vector2 Mouse::GetAbsolutePosition() {
    return absolutePosition;
}

void Mouse::SetAbsolutePositionBounds(unsigned int maxX, unsigned int maxY) {
    absolutePositionBounds.x = float(maxX);
    absolutePositionBounds.y = float(maxY);
}

bool Mouse::WheelMoved() {
    return frameWheel != 0;
}

bool Mouse::DoubleClicked(MouseButtons b) {
    return buttons[b] && doubleClicks[b];
}

int Mouse::GetWheelMovement() {
    return (int)frameWheel;
}

void Mouse::UpdateDoubleClick(float msec) {
    for (auto& pair : lastClickTime) {
        if (pair.second > 0) {
            pair.second -= msec;
            if (pair.second <= 0.0f) {
                doubleClicks[pair.first] = false;
                pair.second = 0.0f;
            }
        }
    }
}

