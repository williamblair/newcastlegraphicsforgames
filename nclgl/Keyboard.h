#ifndef NCLGL_KEYBOARD_H_INCLUDED
#define NCLGL_KEYBOARD_H_INCLUDED

#include <unordered_map>
#include <SDL2/SDL.h>

#include "InputDevice.h"

enum KeyboardKeys
{
    KEYBOARD_TAB = SDLK_TAB,
    KEYBOARD_RETURN = SDLK_RETURN,
    KEYBOARD_ESCAPE = SDLK_ESCAPE,
    KEYBOARD_SPACE = SDLK_SPACE,
    KEYBOARD_LEFT = SDLK_LEFT,
    KEYBOARD_UP = SDLK_UP,
    KEYBOARD_RIGHT = SDLK_RIGHT,
    KEYBOARD_DOWN = SDLK_DOWN,
    KEYBOARD_DELETE = SDLK_DELETE,
    KEYBOARD_0 = SDLK_0,
    KEYBOARD_1 = SDLK_1,
    KEYBOARD_2 = SDLK_2,
    KEYBOARD_3 = SDLK_3,
    KEYBOARD_4 = SDLK_4,
    KEYBOARD_5 = SDLK_5,
    KEYBOARD_6 = SDLK_6,
    KEYBOARD_7 = SDLK_7,
    KEYBOARD_8 = SDLK_8,
    KEYBOARD_9 = SDLK_9,
    KEYBOARD_A = SDLK_a,
    KEYBOARD_B = SDLK_b,
    KEYBOARD_C = SDLK_c,
    KEYBOARD_D = SDLK_d,
    KEYBOARD_E = SDLK_e,
    KEYBOARD_F = SDLK_f,
    KEYBOARD_G = SDLK_g,
    KEYBOARD_H = SDLK_h,
    KEYBOARD_I = SDLK_i,
    KEYBOARD_J = SDLK_j,
    KEYBOARD_K = SDLK_k,
    KEYBOARD_L = SDLK_l,
    KEYBOARD_M = SDLK_m,
    KEYBOARD_N = SDLK_n,
    KEYBOARD_O = SDLK_o,
    KEYBOARD_P = SDLK_p,
    KEYBOARD_Q = SDLK_q,
    KEYBOARD_R = SDLK_r,
    KEYBOARD_S = SDLK_s,
    KEYBOARD_T = SDLK_t,
    KEYBOARD_U = SDLK_u,
    KEYBOARD_V = SDLK_v,
    KEYBOARD_W = SDLK_w,
    KEYBOARD_X = SDLK_x,
    KEYBOARD_Y = SDLK_y,
    KEYBOARD_Z = SDLK_z,
    KEYBOARD_PLUS = SDLK_PLUS,
    KEYBOARD_MINUS = SDLK_MINUS,
    KEYBOARD_SHIFT = SDLK_LSHIFT,
    KEYBOARD_MAX = 1024
};

class Keyboard : public InputDevice
{
public:
    friend class Window;

    bool KeyDown(KeyboardKeys key);
    bool KeyHeld(KeyboardKeys key);
    bool KeyTriggered(KeyboardKeys key);

protected:
    Keyboard();
    virtual ~Keyboard() {}

    virtual void UpdateHolds();
    virtual void Update(const SDL_Event& e);
    virtual void Sleep();

    //bool keyStates[KEYBOARD_MAX];
    //bool holdStates[KEYBOARD_MAX];
    std::unordered_map<KeyboardKeys,bool> keyStates;
    std::unordered_map<KeyboardKeys,bool> holdStates;
};

#endif // NCLGL_KEYBOARD_H_INCLUDED

