#include "MD5Mesh.h"

MD5Mesh::MD5Mesh(const MD5FileData& type) :
    type(type)
{
    tangents = nullptr;
    bumpTexture = 0;
}
MD5Mesh::~MD5Mesh()
{
}

void MD5Mesh::Draw()
{
    if (numVertices == 0) {
        // assume this is our root node
        for (unsigned int i=0; i<children.size(); ++i) {
            children[i]->Draw();
        }
    }
    Mesh::Draw();
}
void MD5Mesh::SkinVertices(const MD5Skeleton& skel)
{
    // for each submesh, we want to transform a position for each vertex
    for (unsigned int i=0; i<type.numSubMeshes; ++i) {
        MD5SubMesh& subMesh = type.subMeshes[i];

        // each md5submesh targets a mesh's data. The first submesh will target
        // this, while subsequent meshes will target the children of this
        MD5Mesh* target = (MD5Mesh*)children.at(i);

        // for each vertex in the submesh, build a final position
        for (int j=0; j<subMesh.numverts; ++j) {
            // UV coords can be copied straight over to the
            // mesh textureCoord array
            target->textureCoords[j] = subMesh.verts[j].texCoords;
            // start with a vector of 0
            target->vertices[j].ToZero();

            // each vertex has a number of weights, determined by weightElements.
            // The first of these weights will be in the submesh weights array,
            // at position weightIndex. Each of these weights has a join it is in
            // relation to, and a weighting value, which determines how much
            // influence the weight has on the final vertex position
            for (int k=0; k<subMesh.verts[j].weightElements; ++k) {
                MD5Weight& weight = subMesh.weights[
                    subMesh.verts[j].weightIndex + k
                ];
                MD5Joint& joint = skel.joints[weight.jointIndex];
                // transform the weight position by the joint's world transform,
                // and multiply the result by the weight value, and add this to
                // the vertex position
                target->vertices[j] +=
                    (joint.transform *
                    weight.position) *
                    weight.weightValue;
            }
        }

        // normals and tangents must be regenerated
        #ifdef MD5_USE_NORMALS
        target->GenerateNormals();
        #endif

        #ifdef MD5_USE_TANGENTS_BUMPMAPS
        target->GenerateTangents();
        #endif

        target->RebufferData();
    }
}

void MD5Mesh::RebufferData()
{
    glBindBuffer(GL_ARRAY_BUFFER, bufferObject[VERTEX_BUFFER]);
    glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector3), (void*)vertices);
    if (textureCoords) {
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject[TEXTURE_BUFFER]);
        glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector2), (void*)textureCoords);
    }

    if (colours) {
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject[COLOUR_BUFFER]);
        glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector4), (void*)colours);
    }

    #ifdef MD5_USE_NORMALS
    if (normals) {
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject[NORMAL_BUFFER]);
        glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector3), (void*)normals);
    }
    #endif

    #ifdef MD5_USE_TANGENTS_BUMPMAPS
    if (tangents) {
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject[TANGENT_BUFFER]);
        glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices*sizeof(Vector3), (void*)tangents);
    }
    #endif

    if (indices) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObject[INDEX_BUFFER]);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numVertices*sizeof(unsigned int), (void*)indices);
    }
}
//void MD5Mesh::BufferExtraData()
//{
//TODO
//}
