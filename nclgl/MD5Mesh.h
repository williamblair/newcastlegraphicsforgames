#pragma once

#include "ChildMeshInterface.h"
#include "Mesh.h"
#include "MD5FileData.h"

class MD5FileData;
class MD5Skeleton;

class MD5Mesh : public Mesh, public ChildMeshInterface
{
public:
    MD5Mesh(const MD5FileData& type);
    virtual ~MD5Mesh();

    virtual void Draw();
    void SkinVertices(const MD5Skeleton& skel);

protected:
    void RebufferData();
    //void BufferExtraData();

    Vector2* weights;
    GLuint weightObject;

    const MD5FileData& type;
};

