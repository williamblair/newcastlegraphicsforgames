#ifndef NCLGL_WINDOW_H_INCLUDED
#define NCLGL_WINDOW_H_INCLUDED

#include <SDL2/SDL.h>
#include <GL/glew.h>

#include "Keyboard.h"
#include "Mouse.h"
#include "OGLRenderer.h"

class OGLRenderer;

class Window
{
friend class OGLRenderer;

public:
    Window(std::string title = "OpenGL Framework", int sizeX = 800, int sizeY = 600, bool fullScreen = false);
    ~Window();

    bool UpdateWindow();
    void SetRenderer(OGLRenderer* r);

    bool HasInitialised();
    void LockMouseToWindow(bool lock);
    void ShowOSPointer(bool show);

    Vector2 GetScreenSize() { return size; }

    static Keyboard* GetKeyboard() { return keyboard; }
    static Mouse* GetMouse() { return mouse; }

    //TODO
    //GameTimer* GetTimer() { return timer; }

protected:
    static Window* window;
    static Keyboard* keyboard;
    static Mouse* mouse;

    //TODO
    //GameTimer* timer;

    SDL_Window* sdlWindow;
    SDL_GLContext sdlContext;

    OGLRenderer* renderer;

    bool forceQuit;
    bool init;
    bool fullScreen;
    bool lockMouse;
    bool showMouse;

    Vector2 position;
    Vector2 size;

    float elapsedMS;
    bool mouseLeftWindow;
};

#endif // NCLGL_WINDOW_H_INCLUDED

