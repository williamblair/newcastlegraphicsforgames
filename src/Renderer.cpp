#include "Renderer.h"

Renderer::Renderer(Window& parent) :
    OGLRenderer(parent)
{
    camera = new Camera(-8,40,Vector3(
        -200,
        50,
        250
    ));
    
    sceneShader = new Shader(SHADERDIR"ShadowSceneVert.glsl", SHADERDIR"ShadowSceneFrag.glsl");
    shadowShader = new Shader(SHADERDIR"ShadowVert.glsl", SHADERDIR"ShadowFrag.glsl");
    if (!sceneShader->LinkProgram()) {
        throw std::runtime_error("Failed to link scene shader");
    }
    if (!shadowShader->LinkProgram()) {
        throw std::runtime_error("Failed to link shadow shader");
    }

    light = new Light(
        Vector3(-450,200,280),
        Vector4(1,1,1,1),
        5500
    );

    hellData = new MD5FileData(MESHDIR"hellknight.md5mesh");
    hellNode = new MD5Node(*hellData);
    hellData->AddAnim(MESHDIR"idle2.md5anim");
    hellNode->PlayAnim(MESHDIR"idle2.md5anim");

    // texture for shadow depth buffer pass
    glGenTextures(1, &shadowTex);
    glBindTexture(GL_TEXTURE_2D, shadowTex);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_DEPTH_COMPONENT,
        SHADOWSIZE, SHADOWSIZE,
        0,
        GL_DEPTH_COMPONENT,
        GL_FLOAT,
        NULL
    );
    glBindTexture(GL_TEXTURE_2D, 0);

    // frame buffer for shadow pass
    glGenFramebuffers(1, &shadowFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadowTex, 0);
    glDrawBuffer(GL_NONE); // don't render to color buffer (speedup)
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    floor = Mesh::GenerateQuad();
    floor->SetTexture(OGLRenderer::LoadGLTextureFromFile(TEXTUREDIR"brick.tga"));
    floor->SetBumpMap(OGLRenderer::LoadGLTextureFromFile(TEXTUREDIR"brickDOT3.tga"));

    projMatrix = Matrix4::Perspective(1.0f, 15000.0f, float(width)/float(height), 45.0f);

    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    init = true;
}

Renderer::~Renderer()
{
    glDeleteTextures(1, &shadowTex);
    glDeleteFramebuffers(1, &shadowFBO);
    delete camera;
    delete light;
    delete hellData;
    delete hellNode;
    delete floor;
    delete sceneShader;
    delete shadowShader;
    currentShader = nullptr;
}

void Renderer::UpdateScene(float msec)
{
    camera->UpdateCamera(msec);
    //viewMatrix = camera->BuildViewMatrix();
    hellNode->Update(msec);
}

void Renderer::RenderScene()
{
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    DrawShadowScene(); // first pass
    DrawCombinedScene(); // second pass

    SwapBuffers();
}

void Renderer::DrawShadowScene()
{
    Matrix4 biasMatrix;
    biasMatrix.ToIdentity();
    biasMatrix.values[0] = 0.5f; // scale
    biasMatrix.values[5] = 0.5f;
    biasMatrix.values[10] = 0.5f;
    biasMatrix.values[12] = 0.5f; // translation
    biasMatrix.values[13] = 0.5f;
    biasMatrix.values[14] = 0.5f;

    glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
    glClear(GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, SHADOWSIZE, SHADOWSIZE);
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    SetCurrentShader(shadowShader);

    viewMatrix = Matrix4::BuildViewMatrix(
        light->GetPosition(),
        Vector3(0,0,0)
    );
    textureMatrix = biasMatrix * (projMatrix*viewMatrix);
    UpdateShaderMatrices();

    DrawFloor();
    DrawMesh();

    glUseProgram(0);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glViewport(0, 0, width, height);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::DrawCombinedScene()
{
    SetCurrentShader(sceneShader);
    glUniform1i(glGetUniformLocation(currentShader->GetProgram(),"diffuseTex"),0);
    glUniform1i(glGetUniformLocation(currentShader->GetProgram(),"bumpTex"),1);
    glUniform1i(glGetUniformLocation(currentShader->GetProgram(),"shadowTex"),2);
    Vector3 camPos(camera->GetPosition());
    glUniform3fv(glGetUniformLocation(currentShader->GetProgram(),"cameraPos"),1,(float*)&camPos);
    SetShaderLight(*light);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, shadowTex);

    viewMatrix = camera->BuildViewMatrix();
    UpdateShaderMatrices();


    DrawFloor();
    DrawMesh();

    glUseProgram(0);
}

void Renderer::DrawMesh()
{
    modelMatrix.ToIdentity();

    Matrix4 tempMatrix = textureMatrix * modelMatrix;
    glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(),"textureMatrix"),1,GL_FALSE,*&tempMatrix.values);
    glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(),"modelMatrix"),1,GL_FALSE,*&modelMatrix.values);

    hellNode->Draw(*this);
}

void Renderer::DrawFloor()
{
    modelMatrix =
        Matrix4::Rotation(90, Vector3(1,0,0)) *
        Matrix4::Scale(Vector3(450,450,1));
    Matrix4 tempMatrix = textureMatrix * modelMatrix;
    glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(),"textureMatrix"),1,GL_FALSE,*&tempMatrix.values);
    glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(),"modelMatrix"),1,GL_FALSE,*&modelMatrix.values);
    floor->Draw();
}

