#include <nclgl/Window.h>
#include "Renderer.h"

#ifdef _WIN32
#undef main
#endif

int main()
{
    Window w("Shadow Mapping!", 800, 600, false);
    if (!w.HasInitialised()) {
        return -1;
    }
    Renderer renderer(w);
    if (!renderer.HasInitialised()) {
        return -1;
    }

    while (w.UpdateWindow() &&
            !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
        renderer.UpdateScene(1000.0f/60.0f);
        renderer.RenderScene();
    }
    return 0;
}

